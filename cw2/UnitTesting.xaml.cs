﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace cw2
{

    public partial class UnitTesting : Window
    {
        public Home home;

        public UnitTesting(Home home)
        {
            this.home = home;
            InitializeComponent();
        }

        private void customers_Click(object sender, RoutedEventArgs e)
        {
            int id1 = Home.newcustRef();
            int id2 = Home.newcustRef();
            int id3 = Home.newcustRef();

            Customer customer1 = new cw2.Customer("Harry Potter", "Hogwarts", id1);
            Home.addCustomer(customer1);
            Home.printCustomer(id1);
            Customer customer2 = new cw2.Customer("Ron Weasly", "Napier University", id2);
            Home.addCustomer(customer2);
            Home.printCustomer(id2);
            Customer customer3 = new cw2.Customer("Hermione Granger", "Edinrbugh University", id3);
            Home.addCustomer(customer3);
            Home.printCustomer(id3);
        }

        private void bookings_Click(object sender, RoutedEventArgs e)
        {
            Booking newbook = new Booking(Convert.ToDateTime("7/15/17"), Convert.ToDateTime("7/21/17"), Home.newbookingRef(), 1);
            newbook.addGuest("Harry Potter", "612626", "16", 1, true, true);
            newbook.addGuest("Ron Weasly", "111111", "16", 1, true, true);
            Home.addBooking(newbook);
            Home.printBooking(newbook.RefNo);

            Booking newbook2 = new Booking(Convert.ToDateTime("8/22/17"), Convert.ToDateTime("8/25/17"), Home.newbookingRef(), 1);
            newbook2.addGuest("Harry Potter", "612626", "16", 1, true, true);
            newbook2.addGuest("Ron Weasly", "111111", "16", 1, true, true);
            newbook2.addGuest("Hermione Granger", "123321", "16", 1, true, true);
            Home.addBooking(newbook2);
            Home.printBooking(newbook2.RefNo);

            Booking newbook3 = new Booking(Convert.ToDateTime("2/16/19"), Convert.ToDateTime("2/24/19"), Home.newbookingRef(), 3);
            newbook3.addGuest("Hermione Granger", "123321", "16", 1, true, true);
            Home.addBooking(newbook3);
            Home.printBooking(newbook3.RefNo);

        }

        private void bookings_Copy_Click(object sender, RoutedEventArgs e)
        {
            BookingsPage bookingspage = new BookingsPage(home);
            bookingspage.Show();
        }
    }
}
