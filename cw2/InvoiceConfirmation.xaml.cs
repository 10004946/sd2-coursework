﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace cw2
{
    public partial class InvoiceConfirmation : Window       //gui class that displays user with interface of invoice relating to their booking
    {
        private Home home;                                      //stores a reference object to the home class for method and list usage                              

        public InvoiceConfirmation(Home home, Booking newbooking) //constructor that creates the interface object to display details about booking 
        {
            this.home = home;
            InitializeComponent();
            custname.Text = newbooking.CustName;
            arrive.Text = (newbooking.ArrivalDate.ToShortDateString()).ToString();
            depart.Text = (newbooking.DepartureDate.ToShortDateString()).ToString();
            if(newbooking.CarHire == true)
            {
                car.Text = "Between: " + newbooking.CarStart.ToShortDateString() + " " + newbooking.CarEnd.ToShortDateString();
            }
            else
            {
                car.Text = "No car hire";
            }
            guest.Text = (newbooking.TotalGuests + newbooking.Guests18).ToString();
            dinner.Text = newbooking.TotalDinners.ToString();
            breakfast.Text = newbooking.TotalBreakfasts.ToString();
            invoice.Text = "£ " + newbooking.Cost().ToString();
            custid.Text = newbooking.CustRef.ToString();
            nonights.Text = ((newbooking.DepartureDate - newbooking.ArrivalDate).Days).ToString();
            roomcost.Text = "£ " + (((newbooking.DepartureDate - newbooking.ArrivalDate).Days) * ((50 * newbooking.TotalGuests) + (30 * newbooking.Guests18))).ToString();
            nightcost.Text = "£ " + ((50 * newbooking.TotalGuests) + (30 * newbooking.Guests18)).ToString();
            carhirecost.Text = "£ " + (50 * (newbooking.CarEnd - newbooking.CarStart).Days);
            breakfastcost.Text = "£ " + ((newbooking.DepartureDate - newbooking.ArrivalDate).Days * newbooking.TotalBreakfasts * 5);
            dinnercost.Text = "£ " + ((newbooking.DepartureDate - newbooking.ArrivalDate).Days * newbooking.TotalDinners * 15);
            extracost.Text = "£ " + (((newbooking.DepartureDate-newbooking.ArrivalDate).Days * newbooking.TotalBreakfasts * 5) + ((newbooking.DepartureDate - newbooking.ArrivalDate).Days * newbooking.TotalDinners * 15) + (50 * (newbooking.CarEnd - newbooking.CarStart).Days));
            Console.WriteLine("Creating new booking with ID -- " + newbooking.RefNo);
            Home.printBooking(newbooking.RefNo);
        }

        private void reserve_Click(object sender, RoutedEventArgs e)        //GUI method to close 
        {
            this.Close();
        }
    }
}
