﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace cw2
{
    public partial class Home : Window //Main window upon launching the application - Dan Gillespie 10/12/2016
    {
        private static List<Customer> customerList = new List<Customer>();                                              //list to store customer objects
        private static List<Booking> bookingList = new List<Booking>();                                                 //list to store booking objects
        public static int CustRef = 0;                                                                                  //customer reference counter
        public static int BookingRef = 0;                                                                               //booking reference counter

        public Home()
        {
            InitializeComponent();
        }
        
        public static void addCustomer(Customer currcust)                                                               //adds a customer to customerList
        {
            customerList.Add(currcust); 
        }

        public static void addBooking(Booking newbooking)                                                               //adds a booking to bookingList
        {
            bookingList.Add(newbooking);
        }

        public static void deleteCustomer(int id)                                                                       //deletes customer from customerlist
        {
            List<int> temp = allbookingsforCust(id);
            foreach (int x in temp)
            {
                deleteBooking(x);
            }

            foreach (Customer b in customerList)
            {
                int x = 0;
                if (id == b.CustRef)
                {
                    customerList.Remove(b);
                    Console.WriteLine("--Removing the Customer: " + b.CustRef + "--");
                    x++;
                }
                if (x != 0) break;
            }
        }

        public static void deleteBooking(int id)                                                                        //deletes booking from bookinglist        
        {
            foreach (Booking b in bookingList)
            {
                int x = 0;
                if (id == b.RefNo)
                {
                    bookingList.Remove(b);
                    Console.WriteLine("--Removing the booking with booking ID: " + b.RefNo + "--");
                    x++;
                }
                if (x != 0) break;
            }
        }

        public static void printcustList()                                                                              //prints entire customerlist
        {

            foreach (Customer c in customerList)
            {
                List<int> bookings = allbookingsforCust(c.CustRef);
                string bookingstring = string.Join(", ", bookings.ToArray());
                Console.WriteLine("--------------------------------------------------------------------------------------------------------------------------");
                Console.WriteLine("Customer Reference: " + c.CustRef + " Customer Name: " + c.CustName + " Customer Address: " + c.CustAddr);
                Console.WriteLine("Bookings made: " + bookings.Count + " Booking IDs: " + bookingstring);
                Console.WriteLine("--------------------------------------------------------------------------------------------------------------------------");
            }
            if (customerList.Count == 0)
            {
                MessageBox.Show("There are no customers in the database :(");
                Console.WriteLine("There are no customers in the database :(");
            }

        }

        public static void printbookingList()                                                                           //prints entire bookinglist
        {
            foreach (Booking c in bookingList)
            {
                Console.WriteLine("Booking ID: " + c.RefNo + " Customer Name: " + c.CustName + " Customer ID: " + c.CustRef + " Number of Guests: " + c.TotalGuests + c.Guests18);
            }
            if (bookingList.Count == 0)
            {
                MessageBox.Show("There are no bookings :(");
                Console.WriteLine("There are no bookings :(");
            }

        }

        public static int newcustRef()                                                                                     //when called returns the next customer reference number
        {
            CustRef++;
            return CustRef;
        }


        public static int newbookingRef()                                                                                   //when called returns the next booking reference number
        {
            BookingRef++;
            return BookingRef;
        }

        public static List<int> allbookingsforCust(int id)                                                                  //returns a list of all bookings a customer has made from their customer id
        {
            List<int> allbookings = new List<int>();
            foreach(Booking b in bookingList)
            {
                if(b.CustRef == id)
                {
                    allbookings.Add(b.RefNo);
                }
            }
            return allbookings;
        }       

        public static void printCustomer(int id)                                                                            //prints a customers details using their id number
        {
            Customer temp = new Customer();
            foreach (Customer c in customerList)
            {
                if(id == c.CustRef)
                {
                    temp = c;
                }
            }
            Console.WriteLine("--------------------------------------------------------------------------------------------------------------------------");
            Console.WriteLine("--New Customer Created--");
            Console.WriteLine("Customer Reference: " + temp.CustRef + " Customer Name: " + temp.CustName + " Customer Address: " + temp.CustAddr);
            Console.WriteLine("--------------------------------------------------------------------------------------------------------------------------");
        }

        public static void printBooking(int id)                                                                             //prints a specific bookings properties from booking id number
        {
            int none = 0;
            int veg = 0;
            int nut = 0;
            Booking temp = new Booking();
            foreach (Booking b in bookingList)
            {
                if (id == b.RefNo)
                {
                    temp = b;
                }
            }
            Console.WriteLine("--------------------------------------------------------------------------------------------------------------------------");
            Console.WriteLine("Printing information regarding BOOKING ID: " + temp.RefNo + ", made by the Customer: " + temp.CustName + " Cust ID: " + temp.CustRef + ".");
            Console.WriteLine(" ");
            Console.WriteLine("Customer Name -- " + temp.CustName);
            Console.WriteLine("Customer ID#: " + temp.CustRef);
            Console.WriteLine("Booking ID#: " + temp.RefNo);
            Console.WriteLine("Number of Nights: " + (temp.DepartureDate - temp.ArrivalDate).Days);
            Console.WriteLine("Arrival Date: " + temp.ArrivalDate.ToShortDateString());
            Console.WriteLine("Departure Date: " + temp.DepartureDate.ToShortDateString());
            Console.WriteLine("Total Guests: " + (temp.TotalGuests + temp.Guests18));
                if(temp.CarHire == true)
                {
                    Console.WriteLine("Car Hire: " + (temp.CarEnd-temp.CarStart).Days + " Days");
                    Console.WriteLine("Driver Name: " + temp.DriverName);
                    Console.WriteLine("Between: " + temp.CarStart);
                    Console.WriteLine("And: " + temp.CarEnd);
                }
                else
                {
                    Console.WriteLine("Car Hire: Not Applicable");
                }
            foreach(Guest g in temp.Guests)
            {
                if (g.Diet == 1)
                {
                    none++;
                }
                if (g.Diet == 2)
                {
                    veg++;
                }
                if (g.Diet == 3)
                {
                    nut++;
                }
            }
                    if(veg > 0 && nut > 0)
                        Console.WriteLine("Dietary Requirements: " + veg + " vegetarian and " + nut + " nut allergy");
                    else if(veg > 0 && nut == 0)
                        Console.WriteLine("Dietary Requirements: " + veg + " vegetarian.");
                    else if (veg == 0 && nut > 0)
                        Console.WriteLine("Dietary Requirements: " + nut + " nut allergy."); 
                    else if (veg == 0 && nut == 0)
                        Console.WriteLine("Dietary Requirements: None.");
            Console.WriteLine("Number of Breakfasts: " + temp.TotalBreakfasts);
            Console.WriteLine("Number of Dinners: " + temp.TotalDinners);
            Console.WriteLine(" ");
            Console.WriteLine("Guest Information ");
            int x = 1;
            foreach (Guest g in temp.Guests)
            {
                Console.WriteLine("Guest " + x + " -- " + g.Name);
                Console.WriteLine("Passport Number: " + g.PassportNo);
                Console.WriteLine("Age: " + g.Age);
                if (g.Diet == 2)
                    Console.WriteLine("Dietary Requirements: Vegetarian");
                else if (g.Diet == 3)
                    Console.WriteLine("Dietary Requirements: Nut Allergy");
                else
                    Console.WriteLine("Dietary Requirements: None");
                if (g.Breakfast == true)
                    Console.WriteLine("Breakfast: Yes");
                else
                    Console.WriteLine("Breakfast: No");
                if (g.Dinner == true)
                    Console.WriteLine("Dinner: Yes");
                else
                    Console.WriteLine("Dinner: No");
                x++;
            }
            Console.WriteLine("--------------------------------------------------------------------------------------------------------------------------");
            nut = 0;
            none = 0;
            veg = 0;
            x = 0;
        }


        public static Booking returnBooking(int id)                                                             //returns a booking object from bookinglist corresponding to the booking id given
        {
            Booking temp = new Booking();
            foreach (Booking b in bookingList)
            {
                if (id == b.RefNo)
                {
                    temp = b;
                }
            }
            return temp;
        }

        public static Customer returnCustomer(int id)                                                           //returns a customer object from customerlist corresponding to the customer id given
        { 
            Customer temp = new Customer();
            foreach (Customer b in customerList)
            {
                if (id == b.CustRef)
                {
                    temp = b;
                }
            }
            return temp;
        }

        public static Guest returnGuest(int bookingid, int guestid)                                             //returns a guest object associated with a booking for a given guestid
        {
            Guest guesttemp = new Guest();
            Booking temp = new Booking();
            foreach (Booking b in bookingList)
            {
                if (bookingid == b.RefNo)
                {
                    temp = b;
                }
            }
            foreach (Guest g in temp.Guests)
            {
                if(guestid == g.GuestId)
                {
                    guesttemp = g;
                }
            }
            return guesttemp;
        }


        private void newcust_Click(object sender, RoutedEventArgs e)
        {
            UnitTesting tester = new UnitTesting(this);
            tester.Show();
        }



        private void reserve_Click(object sender, RoutedEventArgs e)
        {
            NewCustomer cust = new NewCustomer(this);
            cust.Show();
        }

        private void editbooking_Click(object sender, RoutedEventArgs e)
        {
            BookingsPage bookingspage = new BookingsPage(this);
            bookingspage.Show();
        }
    }
}
