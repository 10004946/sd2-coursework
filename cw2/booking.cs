﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cw2
{
    public class Customer                                           //Custome stores details about the customer - Dan Gillespie 10/12/16
    {
        private string _custname;                                   //customers name
        private string _custaddress;                                //customers address
        private int _custref;                                       //reference # of the customer
        public List<int> bookingrefs = new List<int>();             //list of all bookings customer has

        public Customer()                                           //constructor to create empty customer object
        {

        }

        public Customer(string name, string address, int refer)     //constructor to reates customer object with parameter properties
        {
            this._custname = name;
            this._custaddress = address;
            this._custref = refer;
        }

        public string CustName                                          //get/set for customer name
        {
            get { return _custname; }
            set { _custname = value; }
        }
        public int CustRef                                              //get/set for customer reference
        {
            get { return _custref; }
            set { _custref = value; }
        }
        public string CustAddr                                          //get/set for customer name
        {
            get { return _custaddress; }
            set { _custaddress = value; }
        }
    }

    public class Booking : Customer                                 //Booking stores booking properties, includes methods to manipulate these properties - Dan Gillespie 10/12/2016
    {
        private DateTime _arrivaldate;                              //booking arrival date
        private DateTime _departuredate;                            //booking departure date
        private Customer customer;                                  //reference object to customer associated with booking
        private int _referenceno;                                   //booking ID number
        private int dinners = 0;                                    //total number of dinners added onto booking
        private int totalguests = 0;                                //total number of guests over the age of 18
        private int guests18 = 0;                                   //total number of guests either 18 or under
        private int breakfasts = 0;                                 //total number of breakfasts added onto booking
        private int guestcount = 1;                                 //counter to assign ID's to guests
        private bool _carhire;                                      //status of car hire related to booking
        private string _drivername;                                 //name of driver associated with a car hire
        private DateTime _carstart;                                 //date that car hire begins
        private DateTime _carend;                                   //date that car hire ends
        public List<Guest> Guests = new List<Guest>();              //list to store all of the guests associated with booking

        public Booking()                                            //constructor for empty booking object
        {

        }

        public Booking(DateTime arrivaltime, DateTime departtime, int bookref, int custref)                         //constructor for booking object with certain parameters
        {
            this._arrivaldate = arrivaltime;
            this._departuredate = departtime;
            this._referenceno = bookref;
            bookingrefs.Add(bookref);
            this._carhire = false;
            linkCustomer(custref);
            this.CustName = customer.CustName;
            this.CustAddr = customer.CustAddr;
            this.CustRef = customer.CustRef;
        }

        public Booking(DateTime arrivaltime, DateTime departtime, int bookref, int custref, bool carhire)           //constructor to create booking object with certain parameters
        {
            this._arrivaldate = arrivaltime;
            this._departuredate = departtime;
            this._referenceno = bookref;
            this._carhire = carhire;
            bookingrefs.Add(bookref);
            linkCustomer(custref);
            this.CustName = customer.CustName;
            this.CustAddr = customer.CustAddr;
            this.CustRef = customer.CustRef;
        }

        public DateTime ArrivalDate                                                                                 //get/set for arrivaldate
        {
            get { return _arrivaldate; }
            set { _arrivaldate = value; }
        }
        public DateTime DepartureDate                                                                               //get/set for departuredate
        {
            get { return _departuredate; }
            set { _departuredate = value; }
        }
        public int RefNo                                                                                            //get/set for booking reference number
        {
            get { return _referenceno; }
            set { _referenceno = value; }
        }

        public bool CarHire                                                                                         //get/set for car hire status
        {
            get { return _carhire; }
            set { _carhire = value; }
        }

        public string DriverName                                                                                    //get/set for car hire driver name
        {
            get { return _drivername; }
            set { _drivername = value; }
        }

        public DateTime CarStart                                                                                    //get/set for car hire start date
        {
            get { return _carstart; }
            set { _carstart = value; }
        }

        public DateTime CarEnd                                                                                      //get/set for car hire end date
        {
            get { return _carend; }
            set { _carend = value; }
        }

        public int TotalGuests                                                                                      //get/set for number of guests > 18
        {
            get { return totalguests; }
            set { totalguests = value; }
        }
        public int Guests18                                                                                         //get/set for number of guests <= 18
        {
            get { return guests18; }
            set { guests18 = value; }
        }

        public int TotalBreakfasts                                                                                  //get/set for number of total breakfasts associated with booking 
        {
            get { return breakfasts; }
            set { breakfasts = value; }
        }

        public int TotalDinners                                                                                     //get/set for number of dinners associated with booking
        {
            get { return dinners; }
            set { dinners = value; }
        }

        public void linkCustomer(int id)                                                                            //method to link the booking to the customer associated with it 
        {                                                                                                           //sets the reference object customer to the customer object that corresponds to customerid
            this.customer = Home.returnCustomer(id);
        }
        public void addGuest(string name, string passport, string age, int diet, bool dinner, bool breakfast)       //method to create a guest + add the guest to list of guests
        {
            int guestid = guestcount;
            Guest guest1 = new Guest(guestid, name);
            guestcount++;
            guest1.PassportNo = passport;
            guest1.Age = age;
            guest1.Diet = diet;
            guest1.Dinner = dinner;
            guest1.Breakfast = breakfast;
            Guests.Add(guest1);
            if (Int32.Parse(age) <= 18)
            {
                guests18++;
            }
            else
            {
                totalguests++;
            }

            if (breakfast == true)
            {
                breakfasts++;
            }

            if(dinner == true)
            {
                dinners++;
            }         
        }   

        public int Cost()                                                                                               //method to calculate the entire cost of a booking, returns value
        {
            int nightlyrate = 50;
            int nightlyrate18 = 30;
            int breakfastcost = 5 * (breakfasts);
            int dinnercost = 15 * (dinners);
            int carcost = ((_carend - _carstart).Days) * 50;
            int nonights = (_departuredate - _arrivaldate).Days;
            int roomcost = (nonights * totalguests * nightlyrate) + (nonights * guests18 * nightlyrate18);
            int extracost = (breakfastcost + dinnercost + carcost)*nonights;
            int total = roomcost + extracost;
            Console.WriteLine("roomcost: " + roomcost);
            Console.WriteLine("Extracost: " + extracost);
            Console.WriteLine("Total: " + total);
            return total;
        }
    }

    public class Guest                      //Guest stores information relating to each invididual guest - guests are added to a list in a booking object to which they are related
    {

        private string _name;               //stores guest name
        private string _passportno;         //stores guests passport number
        private string _age;                //stores guests age
        private int _dietary;               //stores guests dietary preferences
        private int _guestid;               //stores the guestID number
        private bool _breakfast;            //stores the status of their breakfast order
        private bool _dinner;               //stores the status of their breakfast order

        public Guest()                      //constructor for an empty guest object
        {

        }

        public Guest(int id, string name)   //constructor for a guest from given parameters
        {
            _name = name;
            _guestid = id;
        }

        public string Name                  //get/set for guests name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string PassportNo            //get/set for guests passport number
        {
            get { return _passportno; }
            set { _passportno = value; }
        }

        public string Age                   //get/set for guests age
        {
            get { return _age; }
            set { _age = value; }
        }

        public int GuestId                  //get/set for guestID
        {
            get { return _guestid; }
            set { _guestid = value; }
        }

        public bool Breakfast               //get/set for breakfast status
        {
            get { return _breakfast; }
            set { _breakfast = value; }
        }

        public bool Dinner                  //get/set for dinner status
        {
            get { return _dinner; }
            set { _dinner = value; }
        }

        public int Diet                     //get/set for diet information
        {
            get { return _dietary; }
            set { _dietary = value; }
        }
    }
}
