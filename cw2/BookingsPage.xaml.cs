﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace cw2
{
    /// <summary>
    /// Interaction logic for BookingsPage.xaml
    /// </summary>
    public partial class BookingsPage : Window                                   //GUI class that interfaces viewing/deleteing/editing of bookings -- Dan Gillespie 10/12/16
    {
        public static List<String> diets = new List<String> { "None", "Vegetarian", "Nut Allergy" };        //list of possible diet values
        private Home home;                                                                                  //reference object of the home class (so booking/customer lists can be accessed)
        public int bookingno;                                                                               //stores the booking number of the current booking
        public BookingsPage(Home home)                                                                      //constructor for GUI object
        {
            this.home = home;
            InitializeComponent();
            initilize();
            LoadDiets();
        }

        public void LoadDiets()                                                                             //loads possible diets to the interface from the list of diets
        {
            foreach (string s in diets)
            {
                dietg1.Items.Add(s);
                dietg1.SelectedItem = diets[0];
            }
            foreach (string s in diets)
            {
                dietg2.Items.Add(s);
                dietg2.SelectedItem = diets[0];
            }
            foreach (string s in diets)
            {
                dietg3.Items.Add(s);
                dietg3.SelectedItem = diets[0];
            }
            foreach (string s in diets)
            {
                dietg4.Items.Add(s);
                dietg4.SelectedItem = diets[0];
            }
        }

        public String getDiet(int diet)                                                                     //method to return a string relating to a dietID 
        {
            if (diet == 1)
            {
                return "None";
            }
            if (diet == 2)
            {
                return "Vegetarian";
            }
            if (diet == 3)
            {
                return "Nut Allergy";
            }
            else return "None";
        }

        private void loadbooking_Click(object sender, RoutedEventArgs e)                                    //GUI method to load all bookings for a specified customer in interface
        {   //need a list of cust ref to # of booking ids - cust ref as key
            if (string.IsNullOrWhiteSpace(custidread.Text))
            {
                MessageBox.Show("Please enter a valid customer reference number!");
            }
            else
            {
                Bookings.Items.Clear();
                List<int> bookinglist = new List<int>();
                Booking temp = new Booking();
                int id = Int32.Parse(custidread.Text);
                bookinglist = Home.allbookingsforCust(id);
                foreach (int i in bookinglist)
                {
                    Bookings.Items.Add(Home.returnBooking(i).RefNo);
                }
            }

        }

        private void Bookings_SelectionChanged(object sender, SelectionChangedEventArgs e) //GUI method to load the bookings details to the fields in the interface when a booking is selected       
        {
            initilize();
            if (Bookings.SelectedValue != null)
            {
                string input = Bookings.SelectedValue.ToString();
                int id = Int32.Parse(input);
                Booking temp = new Booking();
                temp = Home.returnBooking(id);
                if (temp.Guests.Count > 0)
                {
                    departdate.Text = temp.DepartureDate.ToString();
                    arrivedate.Text = temp.ArrivalDate.ToString();
                    fnreadg1.Text = temp.Guests[0].Name;
                    passnoreadg1.Text = temp.Guests[0].PassportNo;
                    agereadg1.Text = temp.Guests[0].Age;
                    dietg1.SelectedItem = getDiet(temp.Guests[0].Diet);
                    breakfastg1.IsChecked = temp.Guests[0].Breakfast;
                    dinnerg1.IsChecked = temp.Guests[0].Dinner;
                    if (temp.Guests.Count > 1)
                    {
                        fnreadg2.Text = temp.Guests[1].Name;
                        passnoreadg2.Text = temp.Guests[1].PassportNo;
                        agereadg2.Text = temp.Guests[1].Age;
                        dietg2.SelectedItem = getDiet(temp.Guests[1].Diet);
                        breakfastg2.IsChecked = temp.Guests[1].Breakfast;
                        dinnerg2.IsChecked = temp.Guests[1].Dinner;
                        if (temp.Guests.Count > 2)
                        {
                            fnreadg3.Text = temp.Guests[2].Name;
                            passnoreadg3.Text = temp.Guests[2].PassportNo;
                            agereadg3.Text = temp.Guests[2].Age;
                            dietg3.SelectedItem = getDiet(temp.Guests[2].Diet);
                            breakfastg3.IsChecked = temp.Guests[2].Breakfast;
                            dinnerg3.IsChecked = temp.Guests[2].Dinner;
                            if (temp.Guests.Count > 3)
                            {
                                fnreadg4.Text = temp.Guests[3].Name;
                                passnoreadg4.Text = temp.Guests[3].PassportNo;
                                agereadg4.Text = temp.Guests[3].Age;
                                dietg4.SelectedItem = getDiet(temp.Guests[3].Diet);
                                breakfastg4.IsChecked = temp.Guests[3].Breakfast;
                                dinnerg4.IsChecked = temp.Guests[3].Dinner;
                            }
                        }
                    }
                }
            }
        }

        public void initilize()                                 //method to initialize all interface fields to default
        {
            departdate.Text = "";
            arrivedate.Text = "";
            fnreadg1.Text = "";
            passnoreadg1.Text = "";
            agereadg1.Text = "";
            dietg1.SelectedItem = getDiet(1);
            breakfastg1.IsChecked = false;
            dinnerg1.IsChecked = false;
            fnreadg2.Text = "";
            passnoreadg2.Text = "";
            agereadg2.Text = "";
            dietg2.SelectedItem = getDiet(1);
            breakfastg2.IsChecked = false;
            dinnerg2.IsChecked = false;
            fnreadg3.Text = "";
            passnoreadg3.Text = "";
            agereadg3.Text = "";
            dietg3.SelectedItem = getDiet(1);
            breakfastg3.IsChecked = false;
            dinnerg3.IsChecked = false;
            fnreadg4.Text = "";
            passnoreadg4.Text = "";
            agereadg4.Text = "";
            dietg4.SelectedItem = getDiet(1);
            breakfastg4.IsChecked = false;
            dinnerg4.IsChecked = false;
        }

        private void addbooking_Click(object sender, RoutedEventArgs e)     //GUI method to take user to the newbookings page
        {
            newbooking frombp = new newbooking(home, this);
            frombp.Show();
        }

        public int getDiet(string diet)                                     //method that returns an integer relating to a diet string
        {
            if (diet == "None")
            {
                return 1;
            }
            if (diet == "Vegetarian")
            {
                return 2;
            }
            if (diet == "Nut Allergy")
            {
                return 3;
            }
            else return 1;
        }

        private void alter_Click(object sender, RoutedEventArgs e)          //GUI method that prints the currently selected booking to the command line
        {
            if (string.IsNullOrWhiteSpace(Bookings.SelectedValue.ToString()))
            {
                Booking temp = new Booking();
                temp = Home.returnBooking(Int32.Parse(Bookings.SelectedItem.ToString()));
                Home.printBooking(temp.RefNo);
            }
            else
            {
                MessageBox.Show("There is no booking selected!");
                Console.WriteLine("There is no booking selected!");
            }
        }

        private void delete_Click(object sender, RoutedEventArgs e)         //GUI method that deletes the currently selected booking
        {
            if (string.IsNullOrWhiteSpace(Bookings.SelectedItem.ToString()))
            {
                Console.WriteLine("There is no booking selected!");
                MessageBox.Show("There is no booking selected!");
            }
            else
            {
                Booking temp = new cw2.Booking();
                temp = Home.returnBooking(Int32.Parse(Bookings.SelectedItem.ToString()));
                Home.deleteBooking(Int32.Parse(Bookings.SelectedItem.ToString()));
                Bookings.Items.Clear();
                initilize();
            }
        }

        private void addbooking_Copy_Click(object sender, RoutedEventArgs e)        //GUI method that shows an invoice for a selected booking
        {
            InvoiceConfirmation invoice = new InvoiceConfirmation(home, Home.returnBooking(Int32.Parse(Bookings.SelectedItem.ToString())));
            invoice.Show();
        }

        private void confirmbook_Click(object sender, RoutedEventArgs e)           //GUI method to close page 
        {
            this.Close();
        }

        private void g1save_Click(object sender, RoutedEventArgs e)                 //GUI method to save changes to guest 1
        {
            if (!string.IsNullOrWhiteSpace(fnreadg1.Text) && !string.IsNullOrWhiteSpace(passnoreadg1.Text) && !string.IsNullOrWhiteSpace(agereadg1.Text) && passnoreadg1.Text.Length <= 10 && (Int32.Parse(agereadg1.Text) >= 0 && Int32.Parse(agereadg1.Text) < 101))
            {
                Guest temp = new Guest();
                temp = Home.returnGuest(Int32.Parse(Bookings.SelectedItem.ToString()), 1);
                temp.Name = fnreadg1.Text;
                temp.PassportNo = passnoreadg1.Text;
                temp.Age = agereadg1.Text;
                temp.Diet = getDiet(dietg1.SelectedItem.ToString());
                temp.Breakfast = breakfastg1.IsChecked.Value;
                temp.Dinner = dinnerg1.IsChecked.Value;
            }
            else
            {
                MessageBox.Show("One or many parameters for Guest 1 were not valid for saving");
            }
        }

        private void g2save_Click(object sender, RoutedEventArgs e)                 //GUI method to save changes to guest 2
        {
            if (!string.IsNullOrWhiteSpace(fnreadg2.Text) && !string.IsNullOrWhiteSpace(passnoreadg2.Text) && !string.IsNullOrWhiteSpace(agereadg2.Text) && passnoreadg2.Text.Length <= 10 && (Int32.Parse(agereadg2.Text) >= 0 && Int32.Parse(agereadg2.Text) < 101))
            {
                Guest temp = new Guest();
                temp = Home.returnGuest(Int32.Parse(Bookings.SelectedItem.ToString()), 2);
                Console.WriteLine(temp.Name);
                temp.Name = fnreadg2.Text;
                temp.PassportNo = passnoreadg2.Text;
                temp.Age = agereadg2.Text;
                temp.Diet = getDiet(dietg2.SelectedItem.ToString());
                temp.Breakfast = breakfastg2.IsChecked.Value;
                temp.Dinner = dinnerg2.IsChecked.Value;
            }
            else
            {
                MessageBox.Show("One or many parameters for Guest 2 were not valid for saving");
            }
        }

        private void g3save_Click(object sender, RoutedEventArgs e)                 //GUI method to save changes to guest 3
        {
            if (!string.IsNullOrWhiteSpace(fnreadg3.Text) && !string.IsNullOrWhiteSpace(passnoreadg3.Text) && !string.IsNullOrWhiteSpace(agereadg3.Text) && passnoreadg3.Text.Length <= 10 && (Int32.Parse(agereadg3.Text) >= 0 && Int32.Parse(agereadg3.Text) < 101))
            {
                Guest temp = new Guest();
                temp = Home.returnGuest(Int32.Parse(Bookings.SelectedItem.ToString()), 3);
                temp.Name = fnreadg3.Text;
                temp.PassportNo = passnoreadg3.Text;
                temp.Age = agereadg3.Text;
                temp.Diet = getDiet(dietg3.SelectedItem.ToString());
                temp.Breakfast = breakfastg3.IsChecked.Value;
                temp.Dinner = dinnerg3.IsChecked.Value;
            }
            else
            {
                MessageBox.Show("One or many parameters for Guest 3 were not valid for saving");
            }
        }

        private void g4save_Click(object sender, RoutedEventArgs e)                 //GUI method to save changes to guest 4
        {
            if (!string.IsNullOrWhiteSpace(fnreadg4.Text) && !string.IsNullOrWhiteSpace(passnoreadg4.Text) && !string.IsNullOrWhiteSpace(agereadg4.Text) && passnoreadg4.Text.Length <= 10 && (Int32.Parse(agereadg4.Text) >= 0 && Int32.Parse(agereadg4.Text) < 101))
            {
                Guest temp = new Guest();
                temp = Home.returnGuest(Int32.Parse(Bookings.SelectedItem.ToString()), 4);
                temp.Name = fnreadg4.Text;
                temp.PassportNo = passnoreadg4.Text;
                temp.Age = agereadg4.Text;
                temp.Diet = getDiet(dietg4.SelectedItem.ToString());
                temp.Breakfast = breakfastg4.IsChecked.Value;
                temp.Dinner = dinnerg4.IsChecked.Value;
            }
            else
            {
                MessageBox.Show("One or many parameters for Guest 4 were not valid for saving");
            }
        }

        private void g1delete_Click(object sender, RoutedEventArgs e)                 //GUI method to delete guest 1 from the booking
        {
            Guest temp = new Guest();
            temp = Home.returnGuest(Int32.Parse(Bookings.SelectedItem.ToString()), 1);
            Booking bookingtemp = new cw2.Booking();
            bookingtemp = Home.returnBooking(Int32.Parse(Bookings.SelectedItem.ToString()));
            bookingtemp.Guests.Remove(temp);
        }

        private void g2delete_Click(object sender, RoutedEventArgs e)                 //GUI method to delete guest 2 from the booking
        {
            Guest temp = new Guest();
            temp = Home.returnGuest(Int32.Parse(Bookings.SelectedItem.ToString()), 2);
            Booking bookingtemp = new cw2.Booking();
            bookingtemp = Home.returnBooking(Int32.Parse(Bookings.SelectedItem.ToString()));
            bookingtemp.Guests.Remove(temp);
        }

        private void g3delete_Click(object sender, RoutedEventArgs e)                 //GUI method to delete guest 3 from the booking
        {
            Guest temp = new Guest();
            temp = Home.returnGuest(Int32.Parse(Bookings.SelectedItem.ToString()), 3);
            Booking bookingtemp = new cw2.Booking();
            bookingtemp = Home.returnBooking(Int32.Parse(Bookings.SelectedItem.ToString()));
            bookingtemp.Guests.Remove(temp);
        }

        private void g4delete_Click(object sender, RoutedEventArgs e)                 //GUI method to delete guest 4 from the booking
        {
            Guest temp = new Guest();
            temp = Home.returnGuest(Int32.Parse(Bookings.SelectedItem.ToString()), 4);
            Booking bookingtemp = new cw2.Booking();
            bookingtemp = Home.returnBooking(Int32.Parse(Bookings.SelectedItem.ToString()));
            bookingtemp.Guests.Remove(temp);
        }

        private void deletecust_Click(object sender, RoutedEventArgs e)                 //GUI method to delete the customerID selected and all the bookings associated with it
        {
            if (string.IsNullOrWhiteSpace(custidread.Text))
            {
                MessageBox.Show("Please enter a valid customer reference number!");
            }
            else
            {
                Home.deleteCustomer(Int32.Parse(custidread.Text));
                initilize();
            }
        }

        private void printcustomerlist_Click(object sender, RoutedEventArgs e)                 //GUI method to print the entire customer list
        {
            Home.printcustList();
        }

        private void printbookinglist_Click(object sender, RoutedEventArgs e)                 //GUI method to print the entire booking list
        {
            Home.printbookingList();
        }     
    }
}
