﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace cw2
{
    /// <summary>
    /// Interaction logic for NewCustomer.xaml
    /// </summary>
    public partial class NewCustomer : Window
    {
        private Home home;
        private BookingsPage bookingspage;
        private int custref;

        public NewCustomer(Home home)
        {
            this.home = home;
            InitializeComponent();
        }

        private void newcust_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void newbooking_Click(object sender, RoutedEventArgs e)
        {
            if(fnread.Text.Length == 0)
            {
                MessageBox.Show("Please enter your first name!");
            }
            else if (snread.Text.Length == 0)
            {
                MessageBox.Show("Please enter your surname!");
            }
            else if (adrread.Text.Length == 0)
            {
                MessageBox.Show("Please enter your address!");
            }

            else
            {
                Customer newcust = new Customer(fnread.Text + " " + snread.Text, adrread.Text, Home.newcustRef());
                Home.addCustomer(newcust);
                custref = newcust.CustRef;
                Home.printCustomer(custref);
                MessageBox.Show("Your customer ID is: " + custref.ToString());
                newbooking bookinginput = new newbooking(home, this, custref);
                bookinginput.Show();
                this.Close();
            }
        }

        private void testcase_Click(object sender, RoutedEventArgs e)
        {
            fnread.Text = "Dan";
            snread.Text = "Gillespie";
            emailread.Text = "10004946@live.napier.ac.uk";
            adrread.Text = "14 Kirkhill Terrace";
        }      
    }

}
