﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace cw2
{
    //loading/viewing of bookings 
    public partial class newbooking : Window            //GUI class that interfaces user with the creation of a booking
    {
        private Home home;                              //stores a reference object to the home class for method and list usage
        private NewCustomer cust;                       //stores a reference object to the customer creation page
        private BookingsPage bookingpage;               //stores a reference object to the bookingspage 
        public int custref;                             //stores the customerID from the previous GUI
        public string bookingname;                      //stores the customer name from the previous GUI
        public static List<String> months = new List<String> {"January","February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
        public static List<String> years = new List<String> { "2016", "2017", "2018", "2019" };
        public static List<String> diets = new List<String> { "None", "Vegetarian", "Nut Allergy" }; //Lists that store values to fill the ComboBoxes on the page

        public newbooking(Home home, NewCustomer cust, int custref)   //constructor to create window when coming from newcustomer page
        {
            this.home = home;
            this.cust = cust;
            this.custref = custref;
            this.bookingname = cust.fnread.Text + " " + cust.snread.Text;
            InitializeComponent();
            custid.Text = custref.ToString();
            roomtype.Items.Add("Basic Room");
            roomtype.SelectedItem = "Basic Room";
            loadDates();
            loadDiets();
        }

        public newbooking(Home home, BookingsPage bpag)                 //cosntructor to create window when coming from bookingspage
        {
            this.home = home;
            this.bookingpage = bpag;
            InitializeComponent();
            this.bookingname = bookingpage.fnread.Text + " " + bookingpage.snread.Text;
            this.custref = Int32.Parse(bookingpage.custidread.Text);
            custid.Text = custref.ToString();
            roomtype.Items.Add("Basic Room");
            roomtype.SelectedItem = "Basic Room";
            loadDates();
            loadDiets();
        }

        public void loadDates()                                         //method to load the dates into to fields from the month list
        {
            foreach (string s in months)
            {
                month.Items.Add(s);
                month.SelectedItem = months[0];
            }
            foreach (string i in years)
            {
                year.Items.Add(i);
                year.SelectedItem = years[0];
            }
        }

        public void loadDiets()                                              //method to load all the diets into the fields from the diet lists
        {
            foreach (string s in diets)
            {
                dietg1.Items.Add(s);
                dietg1.SelectedItem = diets[0];
            }
            foreach (string s in diets)
            {
                dietg2.Items.Add(s);
                dietg2.SelectedItem = diets[0];
            }
            foreach (string s in diets)
            {
                dietg3.Items.Add(s);
                dietg3.SelectedItem = diets[0];
            }
            foreach (string s in diets)
            {
                dietg4.Items.Add(s);
                dietg4.SelectedItem = diets[0];
            }
        }

        public void loadbookingDates()                                      //method to load all the booking dates into the fields using lists
        {
            arrivedateread.Items.Clear();
            departdateread.Items.Clear();
            List<DateTime> datelist = new List<DateTime>();
            if (year.SelectedValue != null && month.SelectedValue != null)
            {
                datelist = getDates(Int32.Parse(year.SelectedValue.ToString()), getMonth(month.SelectedValue.ToString()));
            }
            foreach (DateTime dt in datelist)
            {
                arrivedateread.Items.Add(dt);
                arrivedateread.SelectedItem = datelist[0];
                departdateread.Items.Add(dt);
                departdateread.SelectedItem = datelist[0];
            }
        }
        public void loadhireDates()                                         //method to load all the possible dates for car hire depending on the selected values in booking dates
        {                                                                   
            carstartread.Items.Clear();
            carendread.Items.Clear();
            List<DateTime> datelist = new List<DateTime>();
            if (arrivedateread.SelectedValue != null && departdateread.SelectedValue != null)
            {
                DateTime dt = Convert.ToDateTime(arrivedateread.SelectedItem);
                DateTime dt1 = Convert.ToDateTime(departdateread.SelectedItem);
                datelist = gethireDates(dt, dt1);
            }
            foreach (DateTime d in datelist)
            {
                carstartread.Items.Add(d);
                carendread.Items.Add(d);
            }
        }

        public static List<DateTime> getDates(int year, int month)          //creates and returns a list of dates depending on parameters
        {
            var dates = new List<DateTime>();

            for (var date = new DateTime(year, month, 1); date.Month == month; date = date.AddDays(1))
            {
                dates.Add(date);
            }
            return dates;
        }

        public static List<DateTime> gethireDates(DateTime start, DateTime end) //creates and returns a lost of dates depending on parameters
        {
            var dates = new List<DateTime>();

            for(var date = start; date <= end; date = date.AddDays(1))
            {
                dates.Add(date);
            }
            return dates; 
        }       

        public int getMonth(string month)                   //method to return the integer corresponding to a month
        {
            int x = months.IndexOf(month);
            return x + 1;
        }

        public int getDiet(string diet)                     //returns a int corresponding to a diet string as a parameter
        {
            if(diet == "None")
            {
                return 1;
            }
            if(diet == "Vegetarian")
            {
                return 2;
            }
            if (diet == "Nut Allergy")
            {
                return 3;
            }
            else return 1;
        }

        private void addbooking_Click(object sender, RoutedEventArgs e)         //GUI method to prompt the user for confirmaton about the creation of a booking
        {
            if(dataChecks() == true)
            {
                Confirm confirm = new Confirm(home, this);
                confirm.Show();
            }

        }

        private void confirmbook_Click(object sender, RoutedEventArgs e)           //gui method to close the window
        {           
            this.Close();
        }

 
            
        private void year_SelectionChanged(object sender, SelectionChangedEventArgs e)      //gui method that reloads the dates in booking dates field whenever the year is changed
        {
            loadbookingDates();
        }

        private void month_SelectionChanged(object sender, SelectionChangedEventArgs e)     //gui method that reloads the dates in booking dates field whenever the month is changed
        {
            loadbookingDates();
        }

        private void arrivedateread_SelectionChanged(object sender, SelectionChangedEventArgs e)      //gui method that reloads the dates in car hire field whenever the arrivedate field is changed
        {
            loadhireDates();
        }

        private void departdateread_SelectionChanged(object sender, SelectionChangedEventArgs e)      //gui method that reloads the dates in car hire field whenever the departdate field is changed
        {
            loadhireDates();
        }

        private void breakfastg1_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void testcase_Click(object sender, RoutedEventArgs e)  //gui method that loads test date into the fields on click
        {
            month.SelectedItem = "March";
            year.SelectedItem = "2017";    
            g1.IsChecked = true;
            fnreadg1.Text = "Dan Gillespie";
            passnoreadg1.Text = "10002341";
            agereadg1.Text = "24";
            dietg1.SelectedItem = "None";
            breakfastg1.IsChecked = true;
            dinnerg1.IsChecked = true;
            g2.IsChecked = true;
            fnreadg2.Text = "University Student";
            passnoreadg2.Text = "666999";
            agereadg2.Text = "42";
            dietg2.SelectedItem = "Nut Allergy";
            breakfastg2.IsChecked = true;
            dinnerg2.IsChecked = true;
            g3.IsChecked = true;
            fnreadg3.Text = "Depressed Student 3";
            passnoreadg3.Text = "2324242";
            agereadg3.Text = "16";
            dietg3.SelectedItem = "Vegetarian";
            breakfastg3.IsChecked = true;
            dinnerg3.IsChecked = true;
            g4.IsChecked = true;
            fnreadg4.Text = "John Smith";
            passnoreadg4.Text = "1111111";
            agereadg4.Text = "18";
            dietg4.SelectedItem = "None";
            breakfastg4.IsChecked = true;
            dinnerg4.IsChecked = true;
        }

        public void initialize()            //method that initializes all fields on click
        {
            fnreadg1.Text = "";
            passnoreadg1.Text = "";
            g1.IsChecked = false;
            agereadg1.Text = "";
            dietg1.SelectedItem = "";
            breakfastg1.IsChecked = false;
            dinnerg1.IsChecked = false;
            g2.IsChecked = false;
            fnreadg2.Text = "";
            passnoreadg2.Text = "";
            agereadg2.Text = "";
            dietg2.SelectedItem = "";
            breakfastg2.IsChecked = false;
            dinnerg2.IsChecked = false;
            g3.IsChecked = false;
            fnreadg3.Text = "";
            passnoreadg3.Text = "";
            agereadg3.Text = "";
            dietg3.SelectedItem = "";
            breakfastg3.IsChecked = false;
            dinnerg3.IsChecked = false;
            g4.IsChecked = false;
            fnreadg4.Text = "";
            passnoreadg4.Text = "";
            agereadg4.Text = "";
            dietg4.SelectedItem = "";
            breakfastg4.IsChecked = false;
            dinnerg4.IsChecked = false;
        }

        private void initializefields_Click(object sender, RoutedEventArgs e) //gui class to initialize
        {
            initialize();
        }

        public void printError(int guestid, int errorno)  //method to print an error depending on the parameters passed 
        {
            if (errorno == 1)
            {
                MessageBox.Show("Please enter a valid passport number (10 numbers max) for guest " + guestid);
                Console.WriteLine("Please enter a valid passport number (10 numbers max) for guest " + guestid);
            }
            if (errorno == 2)
            {
                MessageBox.Show("Please enter a valid age (Between 0 and 101) for guest " + guestid);
                Console.WriteLine("Please enter a valid age (Between 0 and 101) for guest " + guestid);
            }
            if (errorno == 3)
            {
                MessageBox.Show("Please enter a name for guest " + guestid);
                Console.WriteLine("Please enter a name for guest " + guestid);
            }
            if (errorno == 4)
            {
                MessageBox.Show("Please enter details for at least 1 Guest");
                Console.WriteLine("Please enter details for at least 1 Guest");
            }

        }

        public bool dataChecks()                        //method which checks the validity of fields on the page and returns true if there are no errors
        {
            bool x = true;

            if (g1.IsChecked.Value == false && g2.IsChecked.Value == false && g3.IsChecked.Value == false && g4.IsChecked.Value == false)
            {
                printError(1, 4);
                x = false;
            }

            if (g1.IsChecked.Value == true)
            {
                if (string.IsNullOrWhiteSpace(fnreadg1.Text))
                {
                    printError(1, 3);
                    x = false;
                }
                if (string.IsNullOrWhiteSpace(passnoreadg1.Text))
                {
                    printError(1, 1);
                    x = false;
                }
                else if (passnoreadg1.Text.Length > 10)
                {
                    printError(1, 1);
                    x = false;
                }

                if (string.IsNullOrWhiteSpace(agereadg1.Text))
                {
                    printError(1, 2);
                    x = false;
                }
                else if (Int32.Parse(agereadg1.Text) <= 0 || Int32.Parse(agereadg1.Text) > 101)
                {
                    printError(1, 2);
                    x = false;
                }
            }

            if (g2.IsChecked.Value == true)
            {
                if (string.IsNullOrWhiteSpace(fnreadg2.Text))
                {
                    printError(2, 3);
                    x = false;
                }
                if (string.IsNullOrWhiteSpace(passnoreadg2.Text))
                {
                    printError(2, 1);
                    x = false;
                }
                else if (passnoreadg2.Text.Length > 10)
                {
                    printError(2, 1);
                    x = false;
                }

                if (string.IsNullOrWhiteSpace(agereadg2.Text))
                {
                    printError(2, 2);
                    x = false;
                }
                else if (Int32.Parse(agereadg2.Text) <= 0 || Int32.Parse(agereadg2.Text) > 101)
                {
                    printError(2, 2);
                    x = false;
                }
            }

            if (g3.IsChecked.Value == true)
            {
                if (string.IsNullOrWhiteSpace(fnreadg3.Text))
                {
                    printError(3, 3);
                    x = false;
                }
                if (string.IsNullOrWhiteSpace(passnoreadg3.Text))
                {
                    printError(3, 1);
                    x = false;
                }
                else if (passnoreadg3.Text.Length > 10)
                {
                    printError(3, 1);
                    x = false;
                }
                if (string.IsNullOrWhiteSpace(agereadg3.Text))
                {
                    printError(3, 2);
                    x = false;
                }
                else if (Int32.Parse(agereadg3.Text) <= 0 || Int32.Parse(agereadg3.Text) > 101)
                {
                    printError(3, 2);
                    x = false;
                }
            }
            if (g4.IsChecked.Value == true)
            {
                if (string.IsNullOrWhiteSpace(fnreadg4.Text))
                {
                    printError(4, 3);
                    x = false;
                }
                if (string.IsNullOrWhiteSpace(passnoreadg4.Text))
                {
                    printError(4, 1);
                    x = false;
                }
                else if (passnoreadg4.Text.Length > 10)
                {
                    printError(4, 1);
                    x = false;
                }
                if (string.IsNullOrWhiteSpace(agereadg4.Text))
                {
                    printError(4, 2);
                    x = false;
                }
                else if (Int32.Parse(agereadg4.Text) <= 0 || Int32.Parse(agereadg4.Text) > 101)
                {
                    printError(4, 2);
                    x = false;
                }
            }
            return x;
        }
    }
}
