﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace cw2
{
    /// <summary>
    /// Interaction logic for Confirm.xaml
    /// </summary>
    public partial class Confirm : Window                       //GUI class that prompts the user for confirmation on the creation of a new booking
    {
        private Home home;                                      //stores a reference object to the home class for method and list usage
        private newbooking newbookings;                         //stores a reference object to the newbooking class for data usage

        public Confirm(Home home, newbooking newbooking)        //constructor that creates the interface object
        {
            this.home = home;
            this.newbookings = newbooking;
            InitializeComponent();
            DateTime dt = Convert.ToDateTime(newbookings.arrivedateread.SelectedItem);
            DateTime dt1 = Convert.ToDateTime(newbookings.departdateread.SelectedItem);
            arrive.Text = dt.ToShortDateString().ToString();
            depart.Text = dt1.ToShortDateString().ToString();
            nights.Text = "For " + (dt1 - dt).Days.ToString() + " nights.";
        }

        private void confirm_Click(object sender, RoutedEventArgs e)        //GUI method that confirms the creation of booking objects
        {
            int bookingref = Home.newbookingRef();

            DateTime dt = Convert.ToDateTime(newbookings.arrivedateread.SelectedItem);
            DateTime dt1 = Convert.ToDateTime(newbookings.departdateread.SelectedItem);
            Booking newbook = new Booking(dt, dt1, bookingref, newbookings.custref, newbookings.carhirecheck.IsChecked.Value);
            //newbook.CustName = newbookings.bookingname;
            Home.addBooking(newbook);

            if (newbookings.g1.IsChecked.Value == true)
                newbook.addGuest(newbookings.fnreadg1.Text, newbookings.passnoreadg1.Text, newbookings.agereadg1.Text, newbookings.getDiet(newbookings.dietg1.SelectedValue.ToString()), newbookings.dinnerg1.IsChecked.Value, newbookings.breakfastg1.IsChecked.Value);
            if (newbookings.g2.IsChecked.Value == true)
                newbook.addGuest(newbookings.fnreadg2.Text, newbookings.passnoreadg2.Text, newbookings.agereadg2.Text, newbookings.getDiet(newbookings.dietg2.SelectedValue.ToString()), newbookings.dinnerg2.IsChecked.Value, newbookings.breakfastg2.IsChecked.Value);
            if (newbookings.g3.IsChecked.Value == true)
                newbook.addGuest(newbookings.fnreadg3.Text, newbookings.passnoreadg3.Text, newbookings.agereadg3.Text, newbookings.getDiet(newbookings.dietg3.SelectedValue.ToString()), newbookings.dinnerg3.IsChecked.Value, newbookings.breakfastg3.IsChecked.Value);
            if (newbookings.g4.IsChecked.Value == true)
                newbook.addGuest(newbookings.fnreadg4.Text, newbookings.passnoreadg4.Text, newbookings.agereadg4.Text, newbookings.getDiet(newbookings.dietg4.SelectedValue.ToString()), newbookings.dinnerg4.IsChecked.Value, newbookings.breakfastg4.IsChecked.Value);

            if (newbookings.carhirecheck.IsChecked.Value == true && newbookings.carstartread.SelectedValue != null && newbookings.carendread.SelectedValue != null)
            {
                DateTime carstart = Convert.ToDateTime(newbookings.carstartread.SelectedItem);
                DateTime carend = Convert.ToDateTime(newbookings.carendread.SelectedItem);
                newbook.CarEnd = carend;
                newbook.CarStart = carstart;
                newbook.DriverName = newbookings.driver.Text;
            }
            this.Close();
            InvoiceConfirmation invoice = new InvoiceConfirmation(home, newbook);
            invoice.Show();
        }

        private void return_Click(object sender, RoutedEventArgs e)         //gui method to close page
        {
            this.Close();
        }

    }
}
