﻿#pragma checksum "..\..\Reservation.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "5F93CF9025C649A7C1C9ACE6A03C8134"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using cw2;


namespace cw2 {
    
    
    /// <summary>
    /// Reservation
    /// </summary>
    public partial class Reservation : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 19 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image ENU_logo_01_white_300px_300x941_png;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock title;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock fntext;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox fnread;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock sntext;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox snread;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock emailtext;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox emailread;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox termscheck;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button confirmbook;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock adrtext;
        
        #line default
        #line hidden
        
        
        #line 51 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox adrread;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox placeholder;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox roomtype;
        
        #line default
        #line hidden
        
        
        #line 68 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock roomtypetxt;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock placeholdertxt;
        
        #line default
        #line hidden
        
        
        #line 70 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox arrivedateread;
        
        #line default
        #line hidden
        
        
        #line 78 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock arrivaltxt;
        
        #line default
        #line hidden
        
        
        #line 79 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox departdateread;
        
        #line default
        #line hidden
        
        
        #line 87 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock departtxt1;
        
        #line default
        #line hidden
        
        /// <summary>
        /// guest1txt Name Field
        /// </summary>
        
        #line 88 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.TextBlock guest1txt;
        
        #line default
        #line hidden
        
        
        #line 90 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock departtxt;
        
        #line default
        #line hidden
        
        
        #line 91 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock fntextg1;
        
        #line default
        #line hidden
        
        
        #line 92 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox fnreadg1;
        
        #line default
        #line hidden
        
        
        #line 100 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock sntextg1;
        
        #line default
        #line hidden
        
        
        #line 101 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox snreadg1;
        
        #line default
        #line hidden
        
        
        #line 109 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock passnotxtg1;
        
        #line default
        #line hidden
        
        
        #line 110 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox passnoreadg1;
        
        #line default
        #line hidden
        
        
        #line 118 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock agetxtg1;
        
        #line default
        #line hidden
        
        
        #line 119 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox agereadg1;
        
        #line default
        #line hidden
        
        /// <summary>
        /// guest2txt Name Field
        /// </summary>
        
        #line 127 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.TextBlock guest2txt;
        
        #line default
        #line hidden
        
        
        #line 128 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock fntextg2;
        
        #line default
        #line hidden
        
        
        #line 129 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox fnreadg2;
        
        #line default
        #line hidden
        
        
        #line 137 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock sntextg2;
        
        #line default
        #line hidden
        
        
        #line 138 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox snreadg2;
        
        #line default
        #line hidden
        
        
        #line 146 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock passnotxtg2;
        
        #line default
        #line hidden
        
        
        #line 147 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox passnoreadg2;
        
        #line default
        #line hidden
        
        
        #line 155 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock agetxtg2;
        
        #line default
        #line hidden
        
        
        #line 156 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox agereadg2;
        
        #line default
        #line hidden
        
        /// <summary>
        /// guest3txt Name Field
        /// </summary>
        
        #line 164 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.TextBlock guest3txt;
        
        #line default
        #line hidden
        
        
        #line 165 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock fntextg3;
        
        #line default
        #line hidden
        
        
        #line 166 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox fnreadg3;
        
        #line default
        #line hidden
        
        
        #line 174 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock sntextg3;
        
        #line default
        #line hidden
        
        
        #line 175 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox snreadg3;
        
        #line default
        #line hidden
        
        
        #line 183 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock passnotxtg3;
        
        #line default
        #line hidden
        
        
        #line 184 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox passnoreadg3;
        
        #line default
        #line hidden
        
        
        #line 192 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock agetxtg3;
        
        #line default
        #line hidden
        
        
        #line 193 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox agereadg3;
        
        #line default
        #line hidden
        
        /// <summary>
        /// guest4txt Name Field
        /// </summary>
        
        #line 201 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.TextBlock guest4txt;
        
        #line default
        #line hidden
        
        
        #line 202 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock fntextg4;
        
        #line default
        #line hidden
        
        
        #line 203 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox fnreadg4;
        
        #line default
        #line hidden
        
        
        #line 211 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock sntextg4;
        
        #line default
        #line hidden
        
        
        #line 212 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox snreadg4;
        
        #line default
        #line hidden
        
        
        #line 220 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock passnotxtg4;
        
        #line default
        #line hidden
        
        
        #line 221 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox passnoreadg4;
        
        #line default
        #line hidden
        
        
        #line 229 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock agetxtg4;
        
        #line default
        #line hidden
        
        
        #line 230 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox agereadg4;
        
        #line default
        #line hidden
        
        
        #line 238 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button addbooking;
        
        #line default
        #line hidden
        
        
        #line 239 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox breakfastg1;
        
        #line default
        #line hidden
        
        
        #line 240 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock breakfasttxtg1;
        
        #line default
        #line hidden
        
        
        #line 241 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock dinnertxtg1;
        
        #line default
        #line hidden
        
        
        #line 242 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox dinnerg1;
        
        #line default
        #line hidden
        
        
        #line 243 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox breakfastg3;
        
        #line default
        #line hidden
        
        
        #line 244 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock breakfasttxtg3;
        
        #line default
        #line hidden
        
        
        #line 245 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock dinnertxt3;
        
        #line default
        #line hidden
        
        
        #line 246 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox dinnerg3;
        
        #line default
        #line hidden
        
        
        #line 247 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox breakfastg2;
        
        #line default
        #line hidden
        
        
        #line 248 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock breakfasttxtg2;
        
        #line default
        #line hidden
        
        
        #line 249 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock dinnertxtg2;
        
        #line default
        #line hidden
        
        
        #line 250 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox dinnerg2;
        
        #line default
        #line hidden
        
        
        #line 251 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox breakfastg4;
        
        #line default
        #line hidden
        
        
        #line 252 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock breakfasttxtg4;
        
        #line default
        #line hidden
        
        
        #line 253 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock dinnertxtg4;
        
        #line default
        #line hidden
        
        
        #line 254 "..\..\Reservation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox dinnerg4;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/cw2;component/reservation.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\Reservation.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.ENU_logo_01_white_300px_300x941_png = ((System.Windows.Controls.Image)(target));
            return;
            case 2:
            this.title = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 3:
            this.fntext = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 4:
            this.fnread = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.sntext = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 6:
            this.snread = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.emailtext = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 8:
            this.emailread = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.termscheck = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 10:
            this.confirmbook = ((System.Windows.Controls.Button)(target));
            
            #line 49 "..\..\Reservation.xaml"
            this.confirmbook.Click += new System.Windows.RoutedEventHandler(this.confirmbook_Click);
            
            #line default
            #line hidden
            return;
            case 11:
            this.adrtext = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 12:
            this.adrread = ((System.Windows.Controls.TextBox)(target));
            return;
            case 13:
            this.placeholder = ((System.Windows.Controls.TextBox)(target));
            return;
            case 14:
            this.roomtype = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 15:
            this.roomtypetxt = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 16:
            this.placeholdertxt = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 17:
            this.arrivedateread = ((System.Windows.Controls.TextBox)(target));
            return;
            case 18:
            this.arrivaltxt = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 19:
            this.departdateread = ((System.Windows.Controls.TextBox)(target));
            return;
            case 20:
            this.departtxt1 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 21:
            this.guest1txt = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 22:
            this.departtxt = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 23:
            this.fntextg1 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 24:
            this.fnreadg1 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 25:
            this.sntextg1 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 26:
            this.snreadg1 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 27:
            this.passnotxtg1 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 28:
            this.passnoreadg1 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 29:
            this.agetxtg1 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 30:
            this.agereadg1 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 31:
            this.guest2txt = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 32:
            this.fntextg2 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 33:
            this.fnreadg2 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 34:
            this.sntextg2 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 35:
            this.snreadg2 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 36:
            this.passnotxtg2 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 37:
            this.passnoreadg2 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 38:
            this.agetxtg2 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 39:
            this.agereadg2 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 40:
            this.guest3txt = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 41:
            this.fntextg3 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 42:
            this.fnreadg3 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 43:
            this.sntextg3 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 44:
            this.snreadg3 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 45:
            this.passnotxtg3 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 46:
            this.passnoreadg3 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 47:
            this.agetxtg3 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 48:
            this.agereadg3 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 49:
            this.guest4txt = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 50:
            this.fntextg4 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 51:
            this.fnreadg4 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 52:
            this.sntextg4 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 53:
            this.snreadg4 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 54:
            this.passnotxtg4 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 55:
            this.passnoreadg4 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 56:
            this.agetxtg4 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 57:
            this.agereadg4 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 58:
            this.addbooking = ((System.Windows.Controls.Button)(target));
            
            #line 238 "..\..\Reservation.xaml"
            this.addbooking.Click += new System.Windows.RoutedEventHandler(this.addbooking_Click);
            
            #line default
            #line hidden
            return;
            case 59:
            this.breakfastg1 = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 60:
            this.breakfasttxtg1 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 61:
            this.dinnertxtg1 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 62:
            this.dinnerg1 = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 63:
            this.breakfastg3 = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 64:
            this.breakfasttxtg3 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 65:
            this.dinnertxt3 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 66:
            this.dinnerg3 = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 67:
            this.breakfastg2 = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 68:
            this.breakfasttxtg2 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 69:
            this.dinnertxtg2 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 70:
            this.dinnerg2 = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 71:
            this.breakfastg4 = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 72:
            this.breakfasttxtg4 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 73:
            this.dinnertxtg4 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 74:
            this.dinnerg4 = ((System.Windows.Controls.ComboBox)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

