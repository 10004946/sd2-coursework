This program was my submission for my final grade as a part of coursework for a second year module called Software Development 2. 
The module taught the basics of C# and usage of WPF windows to create basic program UI’s. 

The basis of it was a program that dealt with creating reservations/bookings for a hotel, designed to test usage of classes/objects and basic UI in C#. 

The program is run from Visual Studio 2015 and should initialize to an intuitive homepage which allows the user to carry out tasks such as creating and editing bookings. 
This project was very important to me because for the first time since beginning programming I felt comfortable and able to create and apply my ideas. 
